# How to Contribute to the project

1. First fork the repository
2. Make applicable changes as per needed/wanted
3. Create a pull request back to master with a description of the changes you are making and why
4. When approved I will merge it in - if I do not respond


### Alternative Update Strategies (non-fork)

* I will happily accept patch files. However they must be in the correct Larry Page patch format, and sent clearly via email.
  * Most email clients will mess with plain text so please follow https://www.kernel.org/doc/html/v4.11/process/email-clients.html if there is any confusion

In rare cases I will accept requests for functionality without any submission - but depends on time available and no promises are made, and if any are made I reserve the right to claim ignorance.

