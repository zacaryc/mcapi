from collections import namedtuple
import argparse
import http.client
import json
import logging
import re
import requests
import sys
import time
import uuid
import xml.etree.ElementTree

# requestURL = "https://mcdt9rqgklkwzwbsk1xbx4nfhck1.auth.marketingcloudapis.com/v1/requestToken"

# url = "https://www.exacttargetapis.com/interaction/v1/interactions/20ee0305-af0d-43f3-be90-1e2b50e97f72"

# TODO: Read environment from a file instead of changing in code
# SG
clientId = '7s25757025g88fkkbw31tmzm'
clientSecret = 'bXK7u4B7XCcjwja7P3ufaY3z'

# HK
# clientId = '30aykr4vevlprqhif40edatz'
# clientSecret = 'ToZX6VN6VfB8A9xE062TNKNN'

# Greece TODO: Add in details
# clientId = ''
# clientSecret = ''


environment_data = {
        "restEndpoint": '',
        "soapEndpoint": '',
        "organisationMID": '',
        "userMID": ''
        }


def generate_key():
    """
    Generate a unique key for POST Tokens
    """

    return str(uuid.uuid4())


def requestToken2():
    """
    Request the SFMC token
    """

    authEndpoint = 'https://mcdt9rqgklkwzwbsk1xbx4nfhck1.auth.marketingcloudapis.com/'

    payload = ("{\n    \"clientId\":\"" + clientId +
               "\",\n    \"clientSecret\":\"" + clientSecret + "\"\n}")

    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': generate_key()
    }

    conn = http.client.HTTPSConnection(authEndpoint)
    conn.request("POST", 'v1,requestToken', payload, headers)

    response = conn.getresponse()

    if response.reason != 'OK':
        logging.info("Access Token is: " + response.json().get('accessToken'))
        return conn, response.json().get('accessToken')
    else:
        logging.warning(response.text)
        sys.exit(1)

    return None


def requestToken():
    """
    Request the SFMC token
    """

    authEndpoint = 'https://mcdt9rqgklkwzwbsk1xbx4nfhck1.auth.marketingcloudapis.com/'
    requestURL = authEndpoint + 'v1/requestToken'
    logging.info("Request URL: " + requestURL)

    payload = ("{\n    \"clientId\":\"" + clientId +
               "\",\n    \"clientSecret\":\"" + clientSecret + "\"\n}")

    headers = {
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': generate_key()
    }

    response = requests.request("POST", requestURL,
                                data=payload, headers=headers)

    if response.status_code == requests.codes.ok:
        logging.info("Access Token is: " + response.json().get('accessToken'))
        return response.json().get('accessToken')
    else:
        logging.warning(response.text)
        sys.exit(1)

    return None


def retrieveEndpoints(accessToken):
    """
    Retrieve the endpoints from the token to ensure you are sending data to the
    correct place
    """
    logging.info('Retrieving Endpoints...')
    endpointURL = "https://www.exacttargetapis.com/platform/v1/endpoints/"
    payload = ""
    headers = {
        'Authorization': "Bearer " + accessToken,
        'cache-control': "no-cache",
        'Postman-Token': generate_key()
    }

    response = requests.request("GET", endpointURL,
                                data=payload, headers=headers)

    if response.status_code == requests.codes.ok:
        print()
        # TODO: Code to set Endpoints
    else:
        logging.warning("Bad status response code on endpoint retrieval")
        logging.warning("Response: " + response.text)
        sys.exit(1)

    return None


def listJourneys(accessToken, nameFilter=None):
    """
    Get/List All Journeys
    TODO: Implement Filter function
    """

    url = "https://www.exacttargetapis.com/interaction/v1/interactions"
    payload = ""
    headers = {
        'Authorization': "Bearer " + accessToken,
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': generate_key()
        }
    response = requests.request("GET", url, data=payload, headers=headers)

    # Check response is fine
    if response.status_code != requests.codes.ok:
        logging.warning("Bad status response code on journey retrieval")
        logging.warning("Response: " + response.text)
        return None

    journeyitems = response.json()

    print('========= List of Journeys ==========')
    for k, v in journeyitems.items():
        if k == 'items':
            for journey in v:
                journey_name = journey.get('name')
                if nameFilter is not None:
                    if not re.search(nameFilter[0], journey_name, re.IGNORECASE):
                        continue

                print("|Name: {}\t|id: {} |".format(journey_name,
                      journey.get('id')))


def getJourneyByID(accessToken, id, version=None):
    """
    Retrieve journey details for a given journey id from the API.

    Args:
        accessToken: The access token to validate request to API
        id: A given journey id to retrieve
        version: Specific version of journey to retrieve - default latest

    Returns:
        Returns a json dict of the journey details returned by API
    """

    if id is None:
        print("There has been no id set for an id Lookup")
        return

    # Retrieve the ID via the URL
    url = "https://www.exacttargetapis.com/interaction/v1/interactions/" + id

    if version is not None:
        url += "?versionNumber=" + version[0]

    payload = ""
    headers = {
        'Authorization': "Bearer " + accessToken,
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': generate_key()
    }

    response = requests.request("GET", url, data=payload, headers=headers)

    # Check response is fine
    if response.status_code != requests.codes.ok:
        logging.warning("Bad status response code on journey retrieval")
        logging.warning("Response: " + response.text)
        return None

    return response.json()


def getJourneyHistory(accessToken, id):
    """
        Retrieve Journey History

        Args:
            accessToken: The access token to validate request to API
            id: A given journey id to retrieve

        Returns:
            Returns history of the given journey.
    """

    url = "https://mcdt9rqgklkwzwbsk1xbx4nfhck1.rest.marketingcloudapis.com/interaction/v1/interactions/traceevents/search"

    # Number of return values - craps out if too large
    size = '150'
    startTransactionTime = '2018-11-01'

    payload = ("{\"from\": 0,\"size\": " + size + "," +
               "\"filter\": {\"fquery\": {\"query\": {\"query_string\": " +
               "{\"query\": \"(definitionId:" + id +
               ") AND (transactionTime:[" +
               startTransactionTime + "T13:20:45.101Z TO *])\"}}}}}")
    headers = {
        'Authorization': "Bearer " + accessToken,
        'Content-Type': "application/json",
        'cache-control': "no-cache",
        'Postman-Token': generate_key()
    }

    response = requests.request("POST", url, data=payload, headers=headers)

    # Check response is fine
    if response.status_code != requests.codes.ok:
        logging.warning("Bad status response code on journey history")
        logging.warning("Response: " + response.text)
        return None

    return response.json()


def getOrgHistory():

    return


def reviewJourney(accessToken, id, version=None):
    """
    Get a short summary of important information for a given journey by id.

    Args:
        accessToken: The access token to validate request to API
        id: A given journey id to retrieve
        version: Specific version of journey to retrieve - default latest
    """

    jd = getJourneyByID(accessToken, id, version)

    conditionKeySet = set()

    print("================================================")
    print("Reviewing Journey: {}".format(jd.get('name')))
    print("================================================")
    print("Description: {}".format(jd.get('description')))
    print("Version: {}".format(jd.get('version')))
    print("Status: {}".format(jd.get('status')))
    print("================================================")

    # Get the Goal information
    print("================================================")
    print("Goal Data:")
    print("================================================")
    if len(jd.get('goals')) < 1:
        print("- No Goals Set for this journey")
    else:
        count = 0
        for goal in jd.get('goals'):
            print("Goal #{}".format(count))
            print("Goal Description: {}".format(goal.get('description')))
            print("Goal Criteria: [{}]".format(
                goal.get('metaData').get('configurationDescription')))
            goalConfigurationCriteria = xml.etree.ElementTree.fromstring(
                                                                goal.get('configurationArguments')
                                                                .get('criteria'))
            print("Configuration Keys:")
            for condition in goalConfigurationCriteria.iter('Condition'):
                print('\t- {}'.format(condition.get('Key')))
                conditionKeySet.add(condition.get('Key'))
            count += 1

    # Get the Exit Criteria information
    print("================================================")
    print("Exit Criteria:")
    print("================================================")
    if len(jd.get('exits')) < 1:
        print("- No Exit Criteria set for this journey")
    else:
        count = 0
        for goal in jd.get('exits'):
            print("Exit #{}".format(count))
            print("Exit Description: {}".format(goal.get('description')))
            print("Exit Criteria: [{}]".format(
                goal.get('metaData').get('criteriaDescription')))
            exitConfigurationCriteria = xml.etree.ElementTree.fromstring(
                                                                goal.get('configurationArguments')
                                                                .get('criteria'))
            print("Configuration Keys:")
            for condition in exitConfigurationCriteria.iter('Condition'):
                print('\t- {}'.format(condition.get('Key')))
                conditionKeySet.add(condition.get('Key'))
            count += 1

    print("")
    print("================================================")
    print("Activities:")
    print("================================================")
    activityTypes = set()

    for activity in jd["activities"]:
        atype = activity.get("type")
        key = activity.get("key")
        name = activity.get("name")

        activityTypes.add(atype)

        miscinfo = ''
        if atype == 'WAIT':
            configArgs = activity.get('configurationArguments')
            miscinfo = "{} {}".format(configArgs.get('waitDuration'),
                                      configArgs.get('waitUnit'))
        elif atype == 'EMAILV2':
            sendArgs = activity.get('configurationArguments').get('triggeredSend')
            miscinfo = "'{}' - '{}'".format(sendArgs.get('emailSubject'),
                                            sendArgs.get('preHeader'))
        elif atype == 'SALESCLOUDACTIVITY':
            for object in activity.get('arguments').get('objectMap').get('objects'):
                miscinfo = "{} - {}".format(object.get('action'), object.get('type'))
        elif atype == 'MULTICRITERIADECISION':
            outcomes = activity.get('outcomes')
            miscinfo = str(len(outcomes)) + " paths"

            # Get condition keys
            getFilterCriteria = activity.get('configurationArguments').get('criteria').values()

            for filterCriteria in getFilterCriteria:
                decConfigurationCriteria = xml.etree.ElementTree.fromstring(filterCriteria)
                for condition in decConfigurationCriteria.iter('Condition'):
                    conditionKeySet.add(condition.get('Key'))

        elif atype == 'SMSSYNC':
            miscinfo = activity.get('name')

        # print("|{}| {}|".format(atype, miscinfo))
        print("| {} | {} | {} | {} |".format(atype, miscinfo, name, key))

    # Get a list of all Condition Keys to ensure correct PROD etc
    print("")
    print("================================================")
    print("Data Extensions Used:")
    print("================================================")
    print("")
    for condition in conditionKeySet:
        print("\t- {}".format(condition))

    return


def getJourneyAudit(accessToken, id):
    """
    Get the full audit for the given journey
    """

    url = ("https://www.exacttargetapis.com/interaction/v1/interactions/" +
           id + "/audit/all")

    payload = ""
    headers = {
                'Authorization': "Bearer " + accessToken,
                'Content-Type': "application/json",
                'cache-control': "no-cache",
                'Postman-Token': generate_key()
    }

    response = requests.request("GET", url, data=payload, headers=headers)

    # Check response is fine
    if response.status_code != requests.codes.ok:
        logging.warning("Bad status response code on journey audit")
        logging.warning("Response: " + response.text)
        return None

    # Change single to double quotes since audit returns syntactically
    # incorrect json
    cleanResponse = response.text.replace("'", '"')
    print(cleanResponse)

    return


def main_automation(args):
    """
    Main automation function.
    """

    return


def main_content(args):
    """
    Main content function.
    """

    return


def main_journey(args):
    """
    Main journey function
    """

    if not len(sys.argv) > 1:
        logging.info("Not enough arguments provided")
        sys.exit(0)

    # Any catches for required id
    if not args.id:
        if args.test or args.review or args.history:
            print("You have not provided the required id")
            sys.exit(2)

    # Create Credential Tuples
    # access_credentials = namedtuple("access_credentials", "accessToken
            # urlEndpoint")

    # Get AccessToken
    accessToken = requestToken()
    retrieveEndpoints(accessToken)

    # JOURNEY LIST
    if args.list:
        if args.id:
            # If a specific journey identified,
            # then just show that journeys details
            print(json.dumps(getJourneyByID(accessToken, args.id,
                                            args.version), indent=4))
        else:
            listJourneys(accessToken, args.filter)
        sys.exit(0)

    # JOURNEY REVIEW
    if args.review:
        reviewJourney(accessToken, args.id)

    # JOURNEY AUDIT
    if args.audit:
        getJourneyAudit(accessToken, args.id)

    # JOURNEY DIFF TO PREVIOUS
    # if args.diff and args.id:
        # listJourneys(accessToken, args.id)

    # JOURNEY HISTORY
    if args.history:
        raw = json.loads(getJourneyHistory(accessToken, args.id))

        # TODO: Refactor into a function - this is way too busy
        print(("TransactTime,\tStatus,\tMessage,\tActivityType,\tActivityId," +
              "\tActivityName,\tContactKey"))
        for key in raw.get('hits')['hits']:

            # Only returning up to 10 as there are extra digits that
            # cause epoch conversion to be incorrect
            epochtime = str(key.get('_source').get('transactionTime'))[:10]

            history_line = ("{},{},{},{},{},{},{}"
                  .format(
                        time.strftime('%Y-%m-%d %H:%M:%S',
                                      time.localtime(int(epochtime))),
                        key.get('_source').get('status'),
                        key.get('_source').get('message'),
                        key.get('_source').get('activityType'),
                        key.get('_source').get('activityId'),
                        key.get('_source').get('activityName'),
                        key.get('_source').get('contactKey')))

            if args.filter is not None:
                if not re.search(args.filter[0], history_line, re.IGNORECASE):
                    continue

            print(history_line)

    if args.deploy:
        # TODO: Find target environment - read env.yaml to connect and send
        # details
        # Has target been provided - if not become interactive - also do
        # confirmation
        print("Deploying to environment -> '{}'".format(args.deploy[0]))
        deploy_confirmation = input("Are you sure this is correct (y/n)?  -> ")
        if deploy_confirmation == 'y' or deploy_confirmation == 'Y':
            print("Deploy to {} target confirmed".format(args.deploy))
        else:
            print("Not deploying!")
            sys.exit(2)


if __name__ == '__main__':

    # Setup logging
    # logging.config.fileConfig('logging.conf')
    logging.getLogger(__name__)
    logging.basicConfig(filename='mcapi.log', level=logging.INFO,
                        format='%(asctime)s:%(name)s:%(levelname)s:%(message)s')

    # Start Parser Setup
    parser = argparse.ArgumentParser(description='Tool to review and Parse Marketing Cloud data')
    parser.add_argument('-e', '--env', nargs='?',
                        help='Environment being read from')

    subparsers = parser.add_subparsers(help="Sub Commmand Help", dest='command')
    subjourney = subparsers.add_parser('journey', help="Parse Journey Data")
    subcontent = subparsers.add_parser('content', help="Parse Content Data")
    subautomation = subparsers.add_parser('automation', help="Parse Automation Data")

    # Journeys Group
    journeygroup = subjourney.add_mutually_exclusive_group(required=True)

    # Required Arguments for journey tags - mutually exclusive
    journeygroup.add_argument('-l', '--list', help='List all journeys',
                              action='store_true')
    journeygroup.add_argument('-d', '--deploy', nargs=1, metavar="TARGET",
                              help='Pick an environment to deploy the journey to')
    journeygroup.add_argument('-t', '--test',
                              help='Run test cases over the journey picked',
                              action='store_true')
    journeygroup.add_argument('-r', '--review',
                              help='Review the settings/variables used in a quick report',
                              action='store_true')
    journeygroup.add_argument('--audit',
                              help='See the audit log for the journey',
                              action='store_true')
    journeygroup.add_argument('--diff',
                              help='Diff to previous version',
                              action='store_true')
    journeygroup.add_argument('--history',
                              help='Show the journey history',
                              action='store_true')

    # Extra Args for Journey
    subjourney.add_argument('--version',
                            help='Check specific version',
                            nargs=1, default=None, type=str)
    subjourney.add_argument('--filter', help='Filter the argument given',
                            nargs=1, type=str, default=None)

    # Final Parse
    parser.add_argument('--id', help='Specific Object Id',
                        action='store')

    args = parser.parse_args()

    if args.command == 'journey':
        main_journey(args)
#    if args.content:
#        main_content(args)
#    if args.automation:
#        main_automation(args)

