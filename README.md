#### `generate_key()`

Generate a unique key for POST Tokens

#### `getJourneyAudit(accessToken, id)`

Get the full audit for the given journey

#### `getJourneyByID(accessToken, id, version=None)`

Retrieve journey details for a given journey id from the API.

Args:
accessToken: The access token to validate request to API
id: A given journey id to retrieve
version: Specific version of journey to retrieve - default latest

Returns:
Returns a json dict of the journey details returned by API

#### `getJourneyHistory(accessToken, id)`

Retrieve Journey History

Args:
    accessToken: The access token to validate request to API
    id: A given journey id to retrieve

Returns:
    Returns history of the given journey.

#### `getOrgHistory()`

return


reviewJourney(accessToken, id, version=None):

#### `listJourneys(accessToken, nameFilter=None)`

Get/List All Journeys
TODO: Implement Filter function

#### `main_automation(args)`

Main automation function.

#### `main_content(args)`

Main content function.

#### `main_journey(args)`

Main journey function

#### `requestToken()`

Request the SFMC token

#### `requestToken2()`

Request the SFMC token

#### `retrieveEndpoints(accessToken)`

Retrieve the endpoints from the token to ensure you are sending data to the
correct place

#### `reviewJourney(accessToken, id, version=None)`

Get a short summary of important information for a given journey by id.

Args:
accessToken: The access token to validate request to API
id: A given journey id to retrieve
version: Specific version of journey to retrieve - default latest
